FROM golang:1.17-alpine AS builder

ENV GOPROXY=https://goproxy.cn
WORKDIR /go/src
COPY ./* /go/src/
RUN go build -o httpserver ./main.go

FROM alpine:latest

WORKDIR /root/
COPY --from=builder /go/src/httpserver .
RUN chmod +x /root/httpserver

EXPOSE 80
ENTRYPOINT [ "/root/httpserver" ]