package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	_ "net/http/pprof"

	"gitee.com/alxa-tlh/cn-module10/metrics"
	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/thinkeridea/go-extend/exnet"
)

func main() {
	flag.Parse()
	flag.Set("v", "3")
	glog.V(2).Info("Starting http server...") // 启动时加-logtostderr参数将日志输出到标准输出
	metrics.Register()

	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/healthz", healthz)
	http.Handle("metrics", promhttp.Handler())
	c, python, java := true, false, "no!"
	fmt.Println(c, python, java)
	server := http.Server{Addr: ":80"}

	// 监听系统信号：即将系统信号抽象成os.Signal通道
	signalChan := make(chan os.Signal, 2)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP, syscall.SIGQUIT)
	go func() {
		<-signalChan //此处没有系统信号时阻塞，后续代码不执行，有信号时后续代码开始执行

		signal.Stop(signalChan) //停止监听系统信号
		close(signalChan)       //关闭监听信号的通道
	}()

	idleCloser := make(chan struct{}) //主协程阻塞channel，以便控制http-server优雅退出后才退出主协程
	// 启动一个监听系统信号控制的channel
	go func() {
		<-signalChan

		// 超时context
		timeoutCtx, timeoutCancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer timeoutCancel()

		// 系统包提供的优雅退出方法
		if err := server.Shutdown(timeoutCtx); err != nil {
			glog.Errorf("Http服务暴力停止，一般是达到超时contex的时间当前还有尚未完结的http请求" + err.Error())
		} else {
			glog.Info("Http服务优雅停止")
		}
		// 关闭主进程阻塞channel，本协程安全退出后下面阻塞终止主进程安全退出
		close(idleCloser)
	}()

	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		glog.Error("Http服务异常：" + err.Error())
		close(idleCloser)
	}

	// 通过空结构体channel阻塞主进程达到持续运行的目的
	<-idleCloser
	glog.Info("进程已退出：服务已关闭")

	// mux := http.NewServeMux()
	// mux.HandleFunc("/", rootHandler)
	// mux.HandleFunc("/healthz", healthz)
	// mux.HandleFunc("/debug/pprof/", pprof.Index)
	// mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	// mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	// mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	// err := http.ListenAndServe(":80", mux)

}

func randInt(min int, max int) int {
	rand.Seed((time.Now().UTC().UnixNano()))
	return min + rand.Intn(max-min)
}

func healthz(w http.ResponseWriter, r *http.Request) {
	glog.V(4).Info("entering healthz handler")
	io.WriteString(w, "200\n") // visit /healthz return 200
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	for k, v := range r.Header {
		w.Header().Set(k, strings.Join(v, ",")) // write request header to response header
	}
	version := os.Getenv("VERSION")    // get env VERSION
	w.Header().Set("VERSION", version) //set VERSION to header
	glog.V(2).Info("entering root handler")
	timer := metrics.NewTimer()
	defer timer.ObserveTotal()
	user := r.URL.Query().Get("user")
	delay := randInt(10, 2000)
	time.Sleep(time.Millisecond * time.Duration(delay))
	if user != "" {
		io.WriteString(w, fmt.Sprintf("hello [%s]\n", user))
	} else {
		io.WriteString(w, "hello [stranger]\n")
	}
	io.WriteString(w, "===================Details of the http request header:============\n")
	for k, v := range r.Header {
		io.WriteString(w, fmt.Sprintf("%s=%s\n", k, v))
	}
	ip := exnet.ClientPublicIP(r)
	if ip == "" {
		ip = exnet.ClientIP(r) // get client ip
	}
	code := http.StatusOK
	glog.V(2).Infof("clinet ip is: %s. status code is: %d\n", ip, code) // print client ip and code to stdout
}
